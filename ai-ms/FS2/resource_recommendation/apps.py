from django.apps import AppConfig
from .custom_processes import process_rec_resource

class ResourceRecommendationConfig(AppConfig):
    name = 'resource_recommendation'
    verbose_name = 'FS2 Resource Recommendation'

    def ready(self):
        process_rec_resource()