import csv, os
import pymongo
import sqlite3

here = os.path.dirname(os.path.abspath(__file__))
SQL_DATABASE_PATH = os.path.join(here, "database/UPADatabase.db")
DATABASE_STRING = 'mongodb+srv://haden:iXVXtgPpCDZi5XYZ@cluster0.2cxow.mongodb.net/edunet?retryWrites=true&w=majority'
#DEFAULT_DATABASE = 'UPA'

def store_to_csv(filepath, csv_data):
    with open(os.path.join(here, filepath), 'w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerows(csv_data)

def load_from_csv(filepath, arr, mode='all'):
    if mode == 'columns':
        with open(os.path.join(here, filepath), 'r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            data, column_ids, first = {}, {}, True
            for row in csv_reader:
                if first:
                    for column in arr:
                        for i in range(len(row)):
                            if column == row[i]:
                                column_ids[column] = i
                                data[column] = []
                    first = False
                else:
                    for column in arr:
                        data[column].append(row[column_ids[column]])

            if len(arr) == 1:
                return data[column]
            else:
                return data

def store_to_mongodb(database, collection, data, fields=None, drop_collection=False, mode='csvdata'):
    '''
    database: Name of mongodb database
    collection: Name of mongodb collection
    fields: Names of fields
            [field1_name, field2_name, ..., fieldn_name]
    data: Field values stored in csv format
            [[field1_value1, field2_value1, ..., fieldn_value1]
            [field1_value2, field2_value2, ..., fieldn_value2],
            ...
            [field1_valuem, field2_valuem, ..., fieldn_valuem]]]
    '''
    if mode == 'csvdata':
        docs = []
        for row in data:
            doc = {}
            for i in range(len(fields)):
                doc[fields[i]] = row[i]
            docs.append(doc)
    elif mode == 'mongodata':
        docs = data
    
    client = pymongo.MongoClient(DATABASE_STRING)
    db = client.get_database(database)
    if drop_collection:
        db.drop_collection(collection)
    coll = db.get_collection(collection)
    coll.insert_many(docs)

    client.close()

def transfer_sql_to_mongodb(database, table, drop_collection=False):
    '''
    database: Name of mongodb database
    table: Name of SQL table / name of new mongodb collection
    '''
    conn = sqlite3.connect(SQL_DATABASE_PATH)
    cursor = conn.execute("SELECT * FROM %s" % table)

    fields = [x[0] for x in cursor.description]
    collection = table

    data = []
    rows = cursor.fetchall()
    for row in rows:
        data.append(row)
    docs = []
    for row in data:
        doc = {}
        for i in range(len(fields)):
            doc[fields[i]] = row[i]
        docs.append(doc)

    client = pymongo.MongoClient(DATABASE_STRING)
    db = client.get_database(database)
    if drop_collection:
        db.drop_collection(collection)
    coll = db.get_collection(collection)
    coll.insert_many(docs)

    client.close()

def load_from_mongodb(database, collection, query, details={}, mode='find'):
    '''
    database: Name of mongodb database
    collection: Name of mongodb collection
    query: Query object
            {'field_name': 'field_value'}    
    -------
    returns pymongo.cursor.Cursor instance
    '''
    client = pymongo.MongoClient(DATABASE_STRING)
    db = client.get_database(database)
    coll = db.get_collection(collection)

    if mode == 'find':
        result = coll.find(query, details)
    elif mode == 'distinct':
        result = coll.distinct(query)
    elif mode == 'find_one':
        result = coll.find_one(query, details)
    client.close()
    return result
                
def match(s1, s2):
    # Compare two strings. Allow a difference by one character when returning True.
    ok = False

    for c1, c2 in zip(s1, s2):
        if c1 != c2:
            if ok:
                return False
            else:
                ok = True

    return ok