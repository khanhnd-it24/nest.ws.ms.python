from .backend.recommend import ResourceRecommendation
from confluent_kafka import Consumer, Producer
#from .models import *
import json, datetime, os, time
from dotenv import load_dotenv
    
load_dotenv()

KAFKA_BROKER = os.getenv("KAFKA_BROKER")
TOPIC_CONSUME, TOPIC_PRODUCE = os.getenv("FS2_TOPIC_CONSUME"), os.getenv("FS2_TOPIC_PRODUCE")
CONSUMER_GROUP_ID = os.getenv("CONSUMER_GROUP_ID")
# CLIENT_ID = "kafka-python-fs2-constant"

def process_kp_score(kp_score):
    if len(kp_score) > 5:
        kp_score = sorted(kp_score, key=lambda k: k['score'])[:5]
    return [x['kp'] for x in kp_score]

def dummy_broker():
    producer = producer = Producer({'bootstrap.servers': KAFKA_BROKER})
    producer.produce(TOPIC_CONSUME, b'')
    producer.flush()    

def check_constants():
    print("\n[%s] KAFKA_BROKER_INSTANCE: %s" % (str(datetime.datetime.now()), KAFKA_BROKER), flush=True)
    print("[%s] TOPIC_CONSUME: %s" % (str(datetime.datetime.now()), TOPIC_CONSUME), flush=True)
    print("[%s] TOPIC_PRODUCE: %s" % (str(datetime.datetime.now()), TOPIC_PRODUCE), flush=True)
    print("[%s] CONSUMER_GROUP_ID: %s" % (str(datetime.datetime.now()), CONSUMER_GROUP_ID), flush=True)

def delivery_report(err, msg):
    """ Called once for each message produced to indicate delivery result.
        Triggered by poll() or flush(). """
    if err is not None:
        print("[%s] Reply delivery failed." % (str(datetime.datetime.now())), flush=True)
    else:
        print("[%s] Reply sent: %s..." % (str(datetime.datetime.now()), msg.value().decode()[:40]), flush=True) 

def process_rec_resource():
    print("\n[%s] Initializing process_rec_resource..." % str(datetime.datetime.now()), flush=True)
    # check_constants()

    #while True: 
    # try:
    try: 
        dummy_broker()
    except Exception as e:
        print("[%s] Exception occured: %s" % (str(datetime.datetime.now()), e), flush=True)

    consumer = Consumer({
        'bootstrap.servers': KAFKA_BROKER,
        'group.id': CONSUMER_GROUP_ID,
        'auto.offset.reset': 'latest'
    })
    consumer.subscribe([TOPIC_CONSUME])

    producer = Producer({'bootstrap.servers': KAFKA_BROKER})

    print("\n[%s] Consumer listening..." % str(datetime.datetime.now()), flush=True)  

    try:
        while True:
            message = consumer.poll(1.0)

            print(message)
            if message is None: 
                continue
            if message.error():
                # print("\n[%s] Consumer error: %s..." % (str(datetime.datetime.now()), message.error()), flush=True)
                continue

            msg = message.value().decode("utf-8")
            print("\n[%s] Message received: %s..." % (str(datetime.datetime.now()), msg[:40]), flush=True)

            try:
                kp_score = json.loads(msg)
            except:
                print("[%s] Message not in JSON format. Abort reply process." % str(datetime.datetime.now()), flush=True)
                continue

            weak_kps = process_kp_score(kp_score)
            obj = ResourceRecommendation()
            rec_resource = obj.get_resource_recommendations(weak_kps)

            rec_resource = json.dumps([str(x) for x in rec_resource]) # Convert ObjectId to str
            reply_value = rec_resource.encode()
            reply_key = message.key().decode("utf-8")

            producer.poll(0)
            producer.produce(TOPIC_PRODUCE, reply_value, reply_key, callback=delivery_report)
            producer.flush()

    except KeyboardInterrupt:
        pass

    except Exception as e:
        print("[%s] Exception occured: %s" % (str(datetime.datetime.now()), e), flush=True)

    finally:
        consumer.close()  