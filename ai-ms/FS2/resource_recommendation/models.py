from djongo import models

# Create your models here.
class StudentRecResource(models.Model):
    student_id = models.GenericObjectIdField()
    weak_KPs = models.JSONField()
    rec_resources = models.JSONField()
        
class Resource(models.Model):
    
    class Category(models.IntegerChoices):
        VIDEO = 1
        BOOK = 2

    id = models.GenericObjectIdField(primary_key=True)
    corr_KP = models.JSONField()
    price = models.IntegerField()
    category = models.IntegerField(choices=Category.choices)
    name = models.CharField(max_length=200)
    link = models.URLField()
    rating = models.IntegerField(default=0)  