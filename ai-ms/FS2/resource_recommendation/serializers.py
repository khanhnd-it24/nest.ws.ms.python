from rest_framework import serializers
from .models import StudentRecResource

class StudentRecResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentRecResource
        fields = ('rec_resources',)

class StudentRecResourceDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentRecResource
        fields = '__all__'