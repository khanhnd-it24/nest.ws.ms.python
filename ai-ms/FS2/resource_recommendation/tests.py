from django.test import TestCase
#from kafka import KafkaConsumer, KafkaProducer
from confluent_kafka import Producer, Consumer
#from .models import *
import json, os
from dotenv import load_dotenv
   
load_dotenv()

KAFKA_BROKER = os.getenv("KAFKA_BROKER")
TOPIC_CONSUME, TOPIC_PRODUCE = os.getenv("FS2_TOPIC_CONSUME"), os.getenv("FS2_TOPIC_PRODUCE")
CONSUMER_GROUP_ID = os.getenv("CONSUMER_GROUP_ID")

data = [
    { "kp": "1.9.1.1.1", "score": 3.4 },
    { "kp": "1.9.1.1.2", "score": 4.5 },
    { "kp": "1.9.1.1.3", "score": 8.2 },
    { "kp": "1.9.1.4.8", "score": 6.6 },
    { "kp": "1.9.1.4.7", "score": 3.4 },
    { "kp": "1.9.2.1.1", "score": 9.2 },
    { "kp": "1.9.2.1.2", "score": 8.4 }
]

key = b'507f191e810c19729de860ec'
value = json.dumps(data).encode('utf-8')

# def test1():
#     producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER)
#     try: 
#         # Topic, broker and other config details is in .env
#         # For FS2 message exchange, format should be:
#         # key: <String portion of student ObjectID>
#         # value: kp_score
#         producer.send(TOPIC_CONSUME, b'This is a brand new msg ----------')
#         # producer.send(topic=TOPIC_CONSUME, 
#         #             key=b'507f191e810c19729de860ec', 
#         #             value=json.dumps(data).encode())
#         producer.flush()
#     except Exception as e:
#         print("Exception occurred: %s" % e)

def test2():
    p = Producer({'bootstrap.servers': KAFKA_BROKER})

    def delivery_report(err, msg):
        """ Called once for each message produced to indicate delivery result.
            Triggered by poll() or flush(). """
        if err is not None:
            print('Message delivery failed: {}'.format(err))
        else:
            print('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))

    # Trigger any available delivery report callbacks from previous produce() calls
    p.poll(0)

    # Asynchronously produce a message, the delivery report callback
    # will be triggered from poll() above, or flush() below, when the message has
    # been successfully delivered or failed permanently.
    p.produce(TOPIC_CONSUME, value, key, callback=delivery_report)
    p.produce(TOPIC_CONSUME, b'Helu world')

    # Wait for any outstanding messages to be delivered and delivery report
    # callbacks to be triggered.
    p.flush()

def test3():
    c = Consumer({
        'bootstrap.servers': KAFKA_BROKER,
        'group.id': CONSUMER_GROUP_ID,
        'auto.offset.reset': 'latest'
    })

    c.subscribe([TOPIC_CONSUME])
    try:
        while True:
            msg = c.poll(1.0)

            if msg is None:
                continue
            if msg.error():
                print("Consumer error: {}".format(msg.error()))
                continue
            print('Received message: {}'.format(msg.value().decode('utf-8')))
    except KeyboardInterrupt:
        pass
    finally:
        c.close()

test2()
# test3()