from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import StudentRecResource
from .serializers import *
from .staticinfo import serverlink
from django.urls import reverse

import re
import requests
import json
from .backend.recommend import ResourceRecommendation
from bson.objectid import ObjectId

@api_view(['GET'])
def rec_resourses(request, student_id, weak_KPs):
    """
    Get recommended resources for a student 
    Receive student_id (string) and weak_KPs (string)
    """
    # get all resource list from LMS
    # resource_data = requests.get(serverlink+'api/resource/').json()

    if request.method == 'GET':
        # pass course list to get_course
        tree_id = weak_KPs.split(",")

        obj = ResourceRecommendation()
        rec_resources = obj.get_resource_recommendations(tree_id)
        print(rec_resources)
        
        # save rec course to database 
        resource = StudentRecResource(student_id=ObjectId(student_id), weak_KPs=tree_id, rec_resources=rec_resources)
        resource.save()

        # return the rec resources 
        response_data = StudentRecResourceSerializer(resource).data
        return Response(response_data)
        # return Response(rec_resources)

