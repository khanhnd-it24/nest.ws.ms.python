## Running the application

0. Pre-requisites: docker, docker-compose<br>
1. Run ```git clone https://gitlab.com/hadtech/ai-ms.git ```<br>
2. Run ```docker compose up```. Docker should be logging: <br>
```[2021-06-03 12:00:04.863839] Initializing process_rec_resource...```<br>
```[2021-06-03 12:00:05.191952] Consumer listening...```<br>
3. For testing purposes, run<br>
```cd FS2/resource_recommendation```<br>
```python tests.py```<br>

## Message format

1. LMS to FS2:<br>
```
{
    key: <string portion of student ObjectId>,
    value: <student kp_score>
}
```

2. FS2 to LMS<br>
```
[
    <string portion of resource1 ObjectId>,
    <string portion of resource2 ObjectId>
    ...
]
```
