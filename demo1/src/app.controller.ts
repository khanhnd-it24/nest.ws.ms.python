import { Body, Controller, Get, HttpCode, Inject, Post, Res} from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { ChatGateway } from './chat.gateway';
import { SubscribeTo } from './common/kafka/kafka.decorator';
import { KafkaService } from './common/kafka/kafka.service';

export const responses = Object.create(null);;

@Controller()
export class AppController {
  // constructor(
  //   @Inject('DEMO') private readonly client: ClientKafka,
  //   private readonly chatGateway: ChatGateway,
  //   private readonly kafkaService: KafkaService
  // ){}

  // async onModuleInit() {
  //   this.client.subscribeToResponseOf('say.hello');
  //   this.client.subscribeToResponseOf('ai-student-info');
  //   await this.client.connect();

  //   await this.kafkaService.consumer.run({
  //     eachMessage: async ({ topic, partition, message }) => {
  //       this.chatGateway.sendMessage({key: message.key.toString(), value: message.value.toString()})
  //     },
  //   });
  // }
  
  
  // @Get('test')
  // async testFS2(@Res() res) {
    
    // const value = [
    //   { "kp": "1.9.1.1.1", "score": 3.4 },
    //   { "kp": "1.9.1.1.2", "score": 4.5 },
    //   { "kp": "1.9.1.1.3", "score": 8.2 },
    //   { "kp": "1.9.1.4.8", "score": 6.6 },
    //   { "kp": "1.9.1.4.7", "score": 3.4 },
    //   { "kp": "1.9.2.1.1", "score": 9.2 },
    //   { "kp": "1.9.2.1.2", "score": 8.4 }
    // ];
  //   // console.log(value);
  //   const key = "507f191e810c19729de860ec";
  //   const obj = {
  //       key: key,
  //       value: JSON.stringify(value)
  //   };

  //   this.client.send('ai-student-info', obj).toPromise();
  //   console.log('After')
  //   return 1;
  // }
  
  // @Post()
  // @HttpCode(200)
  // sendAlertToAll(@Body() dto: { key:string, value: any }){
  //   this.chatGateway.sendMessage(dto);
  //   return dto;
  // }

  // @SubscribeTo('ai-rec-resource')
  // helloSubscriber2(payload) {
  //   console.log('[KAKFA-CONSUMER] Print message after receiving', payload);
  //   this.chatGateway.sendMessage(payload);
  // }

}
