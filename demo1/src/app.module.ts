import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AppController } from './app.controller';
import { ChatGateway } from './chat.gateway';
import { KafkaModule } from './common/kafka/kafka.module';

@Module({
  imports: [
    KafkaModule.register({
      clientId: 'ai-ms1-client',
      brokers: ['kafka:9092'],
      groupId: 'ai-consumer',
    }),
    ClientsModule.register([
      {
        name: "DEMO",
        transport: Transport.KAFKA,
        options: {
          client: {
              clientId: 'demo',
              brokers: ['kafka:9092'],
          },
          consumer: {
              groupId: 'ai-consumer'
          }
      }
      },
    ]),
  ],
  controllers: [AppController],
  providers: [ChatGateway],
})
export class AppModule {}
