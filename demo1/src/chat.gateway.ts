import { Inject, Logger } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { OnGatewayInit, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { KafkaService } from './common/kafka/kafka.service';

@WebSocketGateway({ namespace: '/chat' })
export class ChatGateway implements OnGatewayInit{

  constructor(
    @Inject('DEMO') private readonly client: ClientKafka,
    private readonly kafkaService: KafkaService
  ){}
  
  @WebSocketServer() wss: Server;

  private logger: Logger = new Logger('ChatGateway');

  async afterInit(server: any){
    this.logger.log('Initialized!');
    this.client.subscribeToResponseOf('say.hello');
    this.client.subscribeToResponseOf('ai-student-info');
    await this.client.connect();

    await this.kafkaService.consumer.run({
      eachMessage: async ({ topic, partition, message }) => {
        this.sendMessage({key: message.key.toString(), value: message.value.toString()})
      },
    });
  }
  
  @SubscribeMessage('chatToServer')
  receiveMessage( client: Socket, message: { key: string, value: any }){
    const obj = {
      key: message.key,
      value: JSON.stringify(message.value)
    };

    this.client.send('ai-student-info', obj).toPromise();
    console.log('After')
  }

  // @SubscribeMessage('chatToServer')
  sendMessage( message: { key: string, value: any }){
    this.wss.emit('chatToClient', message)
  }

  
}
